fileName = "newhosts"
with open(fileName, "r") as f:
    hostsLines = f.readlines()
    hostsLines = [line.rstrip() for line in hostsLines]

fileName = "dns.log"
with open(fileName, "r") as f:
    dnsLines = f.readlines()

badReqs = 0

for line in dnsLines:
    if (not line.startswith("#")) and (line.split("\t")[9] in hostsLines):
        badReqs += 1 

perc = (badReqs / (sum(not line.startswith("#") for line in dnsLines))) * 100
print("Hosts =", len(dnsLines))
print ("'Bad' hosts  =", perc, "%")
